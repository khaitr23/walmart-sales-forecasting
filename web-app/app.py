import numpy as np
import pandas as pd
from flask import Flask, request, render_template
import pickle

app = Flask(__name__)

model = pickle.load(open("/Users/khaitran/Desktop/Orient_Intern/walmart-sales-forecasting/sales-forecast-lr-model.pkl", "rb"))
scaler_load = pickle.load(open("/Users/khaitran/Desktop/Orient_Intern/walmart-sales-forecasting/scaler.pkl", "rb"))
csv_data = pd.read_csv('./clean_encoded_data.csv', index_col=0)

csv_data_copy = csv_data.copy()
csv_data_copy.drop(['IsHoliday', 'Type', 'Super_Bowl', 'Labor_Day', 'Thanksgiving', 'Christmas', 'Total_MarkDown', 'Lag_1', 'week', 'month', 'year'], inplace=True, axis=1)

predictions = []
latest_date = pd.to_datetime('2012-10-26')

def normalization(df,col):
  for i in col:
    arr = df[i]
    arr = np.array(arr)
    df[i] = scaler_load.transform(arr.reshape(len(arr),1))
  return df

@app.route("/")
def home():
    return render_template("index.html")


@app.route('/predict', methods=['POST'])
def predict():
    store_number = int(request.form.get('store_number'))
    store_department = int(request.form.get('store_department'))
    store_type = int(request.form.get('store_type'))
    total_markdown = request.form.get('total_markdown')
    holiday_type = request.form.get('holiday_type')

    if any(value is None or value == '' for value in [store_number, store_department, store_type, total_markdown, holiday_type]):
        error_message = 'Error: Please fill in all fields.'
        return render_template('index.html', error_message=error_message)
    
    try:
        total_markdown = float(total_markdown)
    except ValueError:
        error_message = 'Error: Invalid input. Please enter numeric values for appropriate fields.'
        return render_template('index.html', error_message=error_message)
    
    is_holiday = True
    super_bowl = 0
    labor_day = 0
    thanksgiving = 0
    christmas = 0

    if holiday_type == 'Super Bowl':
        super_bowl = 1
    elif holiday_type == 'Labor Day':
        labor_day = 1
    elif holiday_type == 'Thanksgiving':
        thanksgiving = 1
    elif holiday_type == 'Christmas':
        christmas = 1
    else:
        is_holiday = False

    global latest_date
    next_week_date = latest_date + pd.Timedelta(days=7)
    latest_date = next_week_date

    lag_val = 0
    rolling_mean = 0 
    if len(predictions) == 0:
        lag_val = csv_data.loc[(csv_data['Store'] == store_number) & (csv_data['Dept'] == store_department), 'Weekly_Sales'].tail(1).values[0]
        rolling_mean = csv_data_copy.loc[(csv_data['Store'] == store_number) & (csv_data_copy['Dept'] == store_department), 'mean_rolling_3mos'].tail(1).values[0]
    else:
        lag_val = predictions[-1].get('predicted_sales')

        new_row = [store_number, store_department, predictions[-1].get('predicted_sales'), 0]
        new_index = (csv_data_copy[(csv_data_copy['Store'] == 1) & (csv_data_copy['Dept'] == 1)].tail(1).index[0]) + 1
        csv_data_copy.loc[new_index] = new_row
        csv_data_copy['mean_rolling_3mos'] = csv_data_copy['Weekly_Sales'].rolling(window=12, center=False, min_periods=0).mean()


    input_data = pd.DataFrame({'Store' : [store_number], 
                               'Dept' : [store_department], 
                               'IsHoliday' : [is_holiday], 
                               'Type' : [store_type], 
                               'Super_Bowl' : [super_bowl], 
                               'Labor_Day' : [labor_day], 
                               'Thanksgiving' : [thanksgiving], 
                               'Christmas' : [christmas], 
                               'Total_MarkDown' : [total_markdown], 
                               'week': [int(next_week_date.week)], 
                               'month' : [int(next_week_date.month)], 
                               'year' : [int(next_week_date.year)], 
                               'Lag_1' : [lag_val], 
                               'mean_rolling_3mos' : [rolling_mean]}, 
                               index=[0])

    scaled_input_data = normalization(input_data.copy(), ['Total_MarkDown', 'Lag_1'])
    predicted_sales = model.predict(scaled_input_data)
    predicted_sales = scaler_load.inverse_transform(predicted_sales.reshape(-1, 1))[0,0]

    prediction = {
        'date' : next_week_date.strftime('%Y-%m-%d'),
        'store_number': store_number,
        'store_department': store_department,
        'store_type': store_type,
        'total_markdown': total_markdown,
        'holiday_type': holiday_type,
        'predicted_sales': predicted_sales
    }
    predictions.append(prediction)
    return render_template("index.html", predictions=predictions,)

if __name__ == "__main__":
    app.run(debug=True)